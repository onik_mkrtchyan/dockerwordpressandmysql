# This is one of my most successful works using Docker.
***
### Creating network WordPress
- docker network create  wordpressNetwork

###  Creating volume for SQL
- docker volume create mysqlVolume

### Creating volume for WordPress
- docker volume create wordpressVolume

### Creating container for SQL DB
- docker run -d --name=db --restart=always -e 'MYSQL_ROOT_PASSWORD=password' \
-e 'MYSQL_DATABASE=wordpress' -e 'MYSQL_USER=wordpress' -e 'MYSQL_PASSWORD=wordpress' \
-v mysqlVolume:/var/lib/mysql mysql

### Creating container for WordPress
- docker run -d --name=wordpress --restart=always -e 'WORDPRESS_DB_HOST=db' \
-e 'WORDPRESS_DB_USER=wordpress' -e 'WORDPRESS_DB_PASSWORD=wordpress' -e 'WORDPRESS_DB_NAME=wordpress' \
-v wordpressVolume:/var/www/html -p 8080:80 wordpress

### Connecting WordPress container with SQL container
- docker network connect wordpressNetwork  wordpress

- docker network connect wordpressNetwork  db
